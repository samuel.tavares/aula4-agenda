package br.com.agenda;

import java.util.Scanner;

public class Main {



    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);
        Utils io = new Utils();

        Utils.imprimirMsg("Bem vindo \n\"Quantidade de contatos?");
        int tamanho = ler.nextInt();

        io.montaMenu();

        Pessoa pessoa = new Pessoa();
        Agenda agenda = new Agenda(tamanho);

        boolean start = true;
        int opcao;

        while(start){
            opcao = ler.nextInt();
            if(opcao == 1){
                System.out.println("Cadastrar");
                pessoa = pessoa.criarPessoa();
                agenda.adicionarContato(pessoa);
                io.montaMenu();
            }else if(opcao == 2){
                System.out.println("Digite nome a ser excluido");
                pessoa =agenda.buscarPorNome(io.coletarNomePessoa());
                if (pessoa != null) {
                    agenda.excluirContato(pessoa);
                }
                io.imprimirMsg("Não encontrado");
                io.montaMenu();
            }else if(opcao == 3){
                System.out.println("Consultar contato");
                pessoa = agenda.buscarPorNome(io.coletarNomePessoa());
                if(pessoa != null){
                    io.imprimirPessoa(pessoa);
                }else {
                    io.imprimirMsg("Não encontrado");
                }
                io.montaMenu();
            }else if(opcao == 4){
                System.out.println("Listar contatos");
                agenda.listarContatos();
                io.montaMenu();
            }else{
                System.out.println("9 para encerrar agenda");
                opcao = ler.nextInt();
                if(opcao == 9){
                    start = false;
                }
            }
        }
    }

}
