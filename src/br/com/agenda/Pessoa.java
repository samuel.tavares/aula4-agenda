package br.com.agenda;

import java.util.Map;

public class Pessoa {
    protected String nome;
    protected String email;
    protected String numero;

    Utils utils = new Utils();
    public Pessoa() {}

    public Pessoa(String nome, String email, String numero) {
        this.nome = nome;
        this.email = email;
        this.numero = numero;
    }

    public Pessoa criarPessoa(){
        Map<String, String> dados = utils.coletaDadosPessoa();
        Pessoa pessoa = new Pessoa(dados.get("nome"),dados.get("email"),dados.get("numCel"));
        return pessoa;
    }
}
