package br.com.agenda;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Utils {

    public static void imprimirMsg (String mensagem){
        System.out.println(mensagem);
    }
    Scanner input = new Scanner(System.in);

    public Map<String, String> coletaDadosPessoa(){
        System.out.println("Digite nome:");
        String nome = input.next();
        System.out.println("Digite email: ");
        String email = input.next();
        System.out.println("Digite telefone:");
        String numCel = input.next();

        Map<String, String> pessoa = new HashMap<>();
        pessoa.put("nome", nome);
        pessoa.put("email", email);
        pessoa.put("numCel", numCel);

        return pessoa;
    }
    public String coletarNomePessoa(){
        System.out.println("Digite o nome:");
        String nome = input.nextLine();
        return nome;
    }

    public void imprimirPessoa(Pessoa pessoa){
        System.out.println("Nome: "+pessoa.nome);
        System.out.println("Email: "+pessoa.email);
        System.out.println("Telefone: "+pessoa.numero);
    }

    public void montaMenu(){
        System.out.println("**********************\n" +
                "Agenda: \n" +
                "1.Cadastrar \n" +
                "2.Excluir \n" +
                "3.Consultar Contato \n" +
                "4.Listar contatos \n" +
                "9.Encerrar");
    }



}
