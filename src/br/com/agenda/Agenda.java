package br.com.agenda;

import java.util.ArrayList;
import java.util.List;

public class Agenda {
    int tamanho;
    List<Pessoa> contatos;

    public Agenda(int tamanho) {
        this.tamanho = tamanho;
        this.contatos = new ArrayList<>(tamanho);
    }

    public void adicionarContato(Pessoa pessoa){
        contatos.add(pessoa);
    }

    public void listarContatos(){
        for (Pessoa pessoa:this.contatos) {
            System.out.println(pessoa.nome);
            System.out.println(pessoa.email);
            System.out.println(pessoa.numero);
        }
    }

    public void excluirContato(Pessoa pessoa) {
        if (contatos.contains(pessoa)) {
            contatos.remove(pessoa);
        }
    }
    public Pessoa buscarPorEmail(String email){
        for (Pessoa pessoa: this.contatos) {
            if (email.equals(pessoa.email)){
                return pessoa;
            }
        }
        return null;
    }
    public Pessoa buscarPorNome(String nome){
        for (Pessoa pessoa:this.contatos) {
            if(nome.equals(pessoa.nome)){
                return pessoa;
            }
        }
        return null;
    }
    public Pessoa buscarPorNumero(String numero){
        for (Pessoa pessoa: this.contatos) {
            if(numero.equals(pessoa.numero)){
                return pessoa;
            }
        }
        return null;
    }
}
